#pragma once

template <class T>
class Propagation {
public:
    Propagation(const QString &message, const QWidget &w, double pixelRatio) :
        message_(message),
        widget_(w),
        x_(0),
        y_(0),
        adjustedX_(0),
        adjustedY_(0),
        pixelRatio_(pixelRatio)
    {
        QStringList chunks(message.split(" "));
        bool isok = false;
        x_ = chunks.at(1).toLong(&isok);
        if (!isok)
            qDebug() << "Extracting x failed";
        y_ = chunks.at(2).toLong(&isok);
        if (!isok)
            qDebug() << "Extracting y failed";
    }

    void exec() {

        QWidget *w = widget_.childAt(QPoint(x_, y_));
        if (!w) {
            qDebug() << "No widget found at " << x_ << " " << y_ << " .. returning";
            return;
        }

        adjustPixelRatio(w);

        return static_cast<T*>(this)->exec(w, adjustedX_, adjustedY_);
    }

    void adjustPixelRatio(const QWidget * const w)
    {
        QPoint offset(w->mapTo(&widget_, QPoint(x_, y_)));

        adjustedX_ -= (offset.x() / pixelRatio_);
        adjustedY_ -= (offset.y() / pixelRatio_);
        adjustedX_ *= pixelRatio_;
        adjustedY_ *= pixelRatio_;
    }

protected:
    const QWidget &widget_;
    const QString &message_;
    long x_;
    long y_;
    long adjustedX_;
    long adjustedY_;
    double pixelRatio_;
};

class MouseMove : public Propagation<MouseMove> {
public:
    void exec(QWidget *w, long x, long y) {
        QApplication::sendEvent(w, new QMouseEvent(QEvent::MouseMove, QPoint(x, y), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier));
    };
};
class MouseDown : public Propagation<MouseDown> {
public:
    void exec(QWidget *w, long x, long y) {
        QApplication::sendEvent(w, new QMouseEvent(QEvent::MouseButtonPress, QPoint(x, y), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier));
    };
};
class MouseUp : public Propagation<MouseUp> {
public:
    void exec(QWidget *w, long x, long y) {
        QApplication::sendEvent(w, new QMouseEvent(QEvent::MouseMove, QPoint(x, y), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier));
        QApplication::sendEvent(w, new QMouseEvent(QEvent::MouseButtonRelease, QPoint(x, y), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier));
    };
};
