#pragma once

#include <QGenericPlugin>
#include <memory>
#include <vector>
#include "qwebcontrollerserverinterface.h"

#include "websocketserv.h"

class QWebControllerServerPlugin : public QGenericPlugin, QWebControllerServerPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QGenericPluginFactoryInterface" FILE "QWebControllerServer.json")
    Q_INTERFACES(QWebControllerServerPluginInterface)

    virtual QObject* create(const QString& name, const QString &spec);

    void start(QApplication &app, QWidget &widget, quint16 port);

public:
    QWebControllerServerPlugin(QObject *parent = 0);

    std::vector<std::unique_ptr<WebSocketServ>> servers_;
};
