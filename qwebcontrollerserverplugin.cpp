#include "qwebcontrollerserverplugin.h"
#include "websocketserv.h"
#include <memory>
#include <QDebug>

QWebControllerServerPlugin::QWebControllerServerPlugin(QObject *parent) :
    QGenericPlugin(parent)
{
}

QObject* QWebControllerServerPlugin::create(const QString& name, const QString &spec)
{
    return static_cast<QObject *>(new QWebControllerServerPlugin);
}

void QWebControllerServerPlugin::start(QApplication &app, QWidget &widget, quint16 port)
{
    //auto server = std::make_unique(new WebSocketServ(app, widget, port));
    auto server = std::unique_ptr<WebSocketServ>(new WebSocketServ(app, widget, port, app.devicePixelRatio()));

    servers_.push_back(std::move(server));
}

