#pragma once

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QByteArray>

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)
QT_FORWARD_DECLARE_CLASS(QApplication)

class WebSocketServ : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketServ(QApplication &app, QWidget &widget, quint16 port, double pixelRatio, QObject *parent = Q_NULLPTR);
    ~WebSocketServ();

    void sendImage(QWebSocket *pClient);
Q_SIGNALS:
    void closed();

private Q_SLOTS:
    void onNewConnection();
    void processTextMessage(QString message);
    void processBinaryMessage(QByteArray message);
    void socketDisconnected();

private:

    double pixelRatio_;

    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;

    QApplication &app_;
    QWidget &widget_;

    QByteArray previousByteArray_;
}
;
