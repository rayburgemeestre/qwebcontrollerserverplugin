#ifndef __QTWEBCONTROLLERSERVERINTERFACE_H__
#define __QTWEBCONTROLLERSERVERINTERFACE_H__
#pragma once

#include <QApplication>

class QWebControllerServerPluginInterface
{
public:
    QWebControllerServerPluginInterface() {}

    virtual void start(QApplication &app, QWidget &widget, quint16 port) = 0;
};


QT_BEGIN_NAMESPACE
#define QWebControllerServerPluginInterface_iid "org.qt-project.Qt.QGenericPluginFactoryInterface"
Q_DECLARE_INTERFACE(QWebControllerServerPluginInterface, QWebControllerServerPluginInterface_iid)
QT_END_NAMESPACE

#endif// __QTWEBCONTROLLERSERVERINTERFACE_H__
