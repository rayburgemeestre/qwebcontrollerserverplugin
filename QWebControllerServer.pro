#-------------------------------------------------
#
# Project created by QtCreator 2014-11-23T19:53:12
#
#-------------------------------------------------

QT       += core gui widgets websockets

TARGET = QWebControllerServer
TEMPLATE = lib
CONFIG += plugin
CONFIG += c++11
CONFIG += c++1y

DESTDIR = $$[QT_INSTALL_PLUGINS]/generic

SOURCES += qwebcontrollerserverplugin.cpp \
    websocketserv.cpp

HEADERS += qwebcontrollerserverplugin.h \
    websocketserv.h \
    propagation.hpp

OTHER_FILES += QWebControllerServer.json

unix {
    target.path = /usr/lib
    INSTALLS += target
}
