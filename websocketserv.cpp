#include "websocketserv.h"

#include <QtWebSockets/QtWebSockets>
#include <QtCore/QDebug>
#include <QMouseEvent>
#include <QApplication>
#include <QWidget>

#include "propagation.hpp"

#include <iostream>
#include <cstdlib>
#include <sstream>

QT_USE_NAMESPACE

WebSocketServ::WebSocketServ(QApplication &app, QWidget &widget, quint16 port, double pixelRatio, QObject *parent) :
    QObject(parent),
    m_pWebSocketServer(new QWebSocketServer(QStringLiteral("WebSocketServ"),
                                            QWebSocketServer::NonSecureMode, this)),
    m_clients(),
    app_(app),
    widget_(widget),
    pixelRatio_(pixelRatio)
{
    if (const char* env_p = std::getenv("PORT")) {
        std::stringstream strValue;
        unsigned int intValue;
        strValue << env_p;
        strValue >> intValue;
        port = intValue;
    }

    if (m_pWebSocketServer->listen(QHostAddress::Any, port)) {
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection, this, &WebSocketServ::onNewConnection);
    }
}

WebSocketServ::~WebSocketServ()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
}

void WebSocketServ::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &WebSocketServ::processTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &WebSocketServ::processBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &WebSocketServ::socketDisconnected);

    m_clients << pSocket;
}

void WebSocketServ::sendImage(QWebSocket *pClient)
{
    QPixmap qPixMap = widget_.grab();
    QImage qImage = qPixMap.toImage();
    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);
    qImage.save(&buffer, "PNG");

    if (ba != previousByteArray_) {
        pClient->sendBinaryMessage(ba);
    }

    previousByteArray_ = QByteArray(ba);
}

void WebSocketServ::processTextMessage(QString message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (!pClient) {
        return;
    }

    if (message == "req_image") {
        sendImage(pClient);
    }
    else if (message.startsWith("req_move_mouse_xy")) {
        Propagation<MouseMove> prop(message, widget_, pixelRatio_);
        prop.exec();
        sendImage(pClient);
    }
    else if (message.startsWith("req_down_mouse_xy")) {
        Propagation<MouseDown> prop(message, widget_, pixelRatio_);
        prop.exec();
        sendImage(pClient);
    }
    else if (message.startsWith("req_up_mouse_xy")) {
        Propagation<MouseUp> prop(message, widget_, pixelRatio_);
        prop.exec();
        sendImage(pClient);
    }
}

void WebSocketServ::processBinaryMessage(QByteArray message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient) {
        pClient->sendBinaryMessage(message);
    }
}

void WebSocketServ::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient) {
        m_clients.removeAll(pClient);
        pClient->deleteLater();
    }
}
