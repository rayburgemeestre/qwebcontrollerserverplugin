#ifndef __QTWEBCONTROLLERSERVER_H__
#define __QTWEBCONTROLLERSERVER_H__
#pragma once

#include <QApplication>
#include <QWidget>
//#include <QtCore/QObject>

//QT_FORWARD_DECLARE_CLASS(QApplication)

#include <QPluginLoader>
#include <QDebug>

class QtWebControllerServer : public QObject
{
public:
    QtWebControllerServer() {}

    virtual int start(QApplication &app, QWidget &widget, quint16 port);
};

#endif // __QTWEBCONTROLLERSERVER_H__
